(asdf:defsystem #:cl-propagators
  :description "Propagators in Common Lisp"
  :author "Mariano Montone <mariano.montone@gmail.com>"
  :license "MIT"
  :serial t
  :depends-on (:cl-dot :alexandria :cl-fad :inferior-shell)
  :components ((:file "package")
               (:file "propagators")
               (:file "draw")))
