(in-package :propagators)

(defstruct propagation-graph
  (show-cell-name t)
  (show-cell-content t)
  (show-cell-type nil))

(defmethod cl-dot:graph-object-node ((graph propagation-graph)
                                     (cell cell))
  (let ((node-label (format nil "~@[~A: ~]~@[~A~]"
                            (and (propagation-graph-show-cell-name graph)
                                 (cell-name cell))
                            (and (propagation-graph-show-cell-content graph)
                                 (not (nothing-p (content cell)))
                                 (content cell)))))
    (make-instance 'cl-dot::node
                   :attributes (list :label node-label
                                     :shape :oval
                                     :style :filled
                                     :fillcolor "#eeeeff"))))

(defmethod cl-dot:graph-object-pointed-to-by ((graph propagation-graph)
                                              (cell cell))
  (alexandria:flatten
   (mapcar (lambda (propagator)
             (remove nil
                     (cons (output propagator) (neighbors propagator))))
           (neighbors cell))))

(defun draw-graph (cell pathname &optional (graph-type (make-propagation-graph)))
  (with-open-file (f pathname
                     :direction :output
                     :if-does-not-exist :create
                     :if-exists :supersede)
    (draw-graph-to-stream cell f graph-type)))

(defun draw-graph-to-stream (cell stream &optional (graph-type (make-propagation-graph)))
  (cl-dot:print-graph
   (cl-dot:generate-graph-from-roots graph-type (list cell))
   :stream stream
   :directed nil))

(defun show-graph (cell &optional (graph-type (make-propagation-graph)))
  (let ((tmp-pathname (cl-fad::generate-random-pathname "/tmp/props%.dot" 'cl-fad::generate-random-string))
        (png-pathname (cl-fad::generate-random-pathname "/tmp/props%.png" 'cl-fad::generate-random-string)))
    (draw-graph cell tmp-pathname graph-type)
    (inferior-shell:run (format nil "dot ~A -Tpng -o ~A" tmp-pathname png-pathname))
    (inferior-shell:run (format nil "xdg-open ~A" png-pathname))))
