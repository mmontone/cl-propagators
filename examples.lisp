(in-package :propagators)

(defun fahrenheit->celsius (f c)
  (let ((thirty-two (make-cell :name 'thirty-two))
        (f-32 (make-cell :name 'f-32))
        (five (make-cell :name 'five))
        (c*9 (make-cell :name 'c*9))
        (nine (make-cell :name 'nine)))
    (add-content thirty-two 32)
    (add-content five 5)
    (add-content nine 9)
    (substractor f thirty-two f-32)
    (multiplier f-32 five c*9)
    (divider c*9 nine c)))

(defun fahrenheit->celsius-2 (f c)
  (let ((thirty-two (make-cell :name 'thirty-two :content 32))
        (f-32 (make-cell :name 'f-32))
        (five (make-cell :name 'five :content 5))
        (c*9 (make-cell :name 'c*9))
        (nine (make-cell :name 'nine :content 9)))
    (substractor f thirty-two f-32)
    (multiplier f-32 five c*9)
    (divider c*9 nine c)))

(defun fahrenheit->celsius-3 (f c)
  (let-cells
      ((thirty-two 32)
       f-32 
       (five 5)
       c*9
       (nine 9))
    (substractor f thirty-two f-32)
    (multiplier f-32 five c*9)
    (divider c*9 nine c)))

;;-----------

(initialize-scheduler)

(define-cell f)
(define-cell c)

(fahrenheit->celsius f c)

(add-content f 77)

(run)

(content c)

;;(show-graph f)

;;----

(defstruct interval
  low high)

(defun mk-interval (low high)
  (make-interval :low low :high high))

(defun mul-interval (x y)
  (mk-interval (* (interval-low x) (interval-low y))
               (* (interval-high x) (interval-high y))))

(defun div-interval (x y)
  (mul-interval x (mk-interval (/ 1.0 (interval-high y))
                               (/ 1.0 (interval-low y)))))

(defun square-interval (x)
  (mk-interval (* (interval-low x) (interval-low x))
               (* (interval-high x) (interval-high x))))

(defun sqrt-interval (x)
  (mk-interval (sqrt (interval-low x))
               (sqrt (interval-high x))))

(defun empty-interval-p (x)
  (> (interval-low x) (interval-high x)))

(defun intersect-intervals (x y)
  (mk-interval (max (interval-low x) (interval-low y))
               (min (interval-high x) (interval-high y))))

(define-propagator-function int-multiplier mul-interval)
(define-propagator-function int-divider div-interval)
(define-propagator-function int-squarer square-interval)
(define-propagator-function int-sqrter sqrt-interval)

(defmethod merge-content ((cell cell) (content interval) (increment interval))
  (let ((new-range (intersect-intervals content increment)))
    (cond
      ((equalp content increment)
       content)
      ((empty-interval-p new-range)
       (error "Inconsistency"))
      (t new-range))))

;; Allow intervals and numbers to play together

(defun ensure-inside-interval (number interval)
  (if (<= (interval-low interval) number (interval-high interval))
      number
      (error "Inconsistency")))

(defmethod merge-content ((cell cell) (content interval) (increment number))
  (ensure-inside increment content))

(defmethod merge-content ((cell cell) (content number) (increment interval))
  (ensure-inside content increment))


;; TODO: implement generic-operations (generic-+, generic-*, etc), that handle
;; both numbers and intervals

;;-- Barometer example

(defun fall-duration (time h)
  (let-cells ((g (mk-interval 9.789 9.832))
              (one-half (mk-interval 1/2 1/2))
              t^2
              gt^2)
    (int-squarer time t^2)
    (int-multiplier g t^2 gt^2)
    (int-multiplier one-half gt^2 h)))

(initialize-scheduler)
    
(define-cell fall-time (mk-interval 2.9 3.1))
(define-cell building-height)

(fall-duration fall-time building-height)

(run)

(content building-height)

;; (show-graph fall-time)

;;-----
;;-- Multidirectional example

(defun sum (x y total)
  (adder x y total)
  (substractor total x y)
  (substractor total y x))

(defun product (x y total)
  (multiplier x y total)
  (divider total x y)
  (divider total y x))

(defun quadratic (x x^2)
  (squarer x x^2)
  (sqrter x^2 x))

(initialize-scheduler)

(define-cell x 22)
(define-cell total 45)
(define-cell y)

(sum x y total)

(run)

(content y)

(initialize-scheduler)

(define-cell x 22)
(define-cell total)
(define-cell y 23)

(sum x y total)

(run)

(content total)

;;-- Multidirectional Fahrenheit

(defun fahrenheit<->celsius (f c)
  (let ((thirty-two (make-cell :name 'thirty-two))
        (f-32 (make-cell :name 'f-32))
        (five (make-cell :name 'five))
        (c*9 (make-cell :name 'c*9))
        (nine (make-cell :name 'nine)))
    (add-content thirty-two 32)
    (add-content five 5)
    (add-content nine 9)
    (sum thirty-two f-32 f)
    (product f-32 five c*9)
    (product c nine c*9)))

(initialize-scheduler)

(define-cell f)
(define-cell c)

(fahrenheit<->celsius f c)

(add-content c 25)

(run)

(content f)


(initialize-scheduler)

(define-cell f)
(define-cell c)

(fahrenheit<->celsius f c)

(add-content f 77)

(run)

(content c)

;;-- Combined network

(defun celsius<->kelvin (c k)
  (let-cells ((many 273.15))
    (sum c many k)))

(initialize-scheduler)

(define-cell f (nothing) :test 'equalp) ;; We need equalp because of float <-> integer comparisons
(define-cell c (nothing) :test 'equalp)
(define-cell k (nothing) :test 'equalp)

(fahrenheit<->celsius f c)
(celsius<->kelvin c k)

(add-content f 77)

(run)

(content k)

;; Idea: allow free variables (free variables are assigned a new cell)

;; Example:

;; ;;F, C and K are  free variable (a fresh cell)
;; (with-free-cells 
;;   (fahrenheit<->celsius f c)
;;   (celsius<->kelvin c k))

;; Another idea: treat free variables as input cells on propagator definitions

;;-- Reactive cells

(initialize-scheduler 'reactive-scheduler)

(define-cell ui-text (nothing) :type 'reactive-cell
             :test 'equalp)
(define-cell model-text (nothing) :type 'reactive-cell
             :test 'equalp)
(define-cell filled-text (nothing) :type 'reactive-cell)

(propagate (model-text) filled-text
  (> (length model-text) 0))

(propagate (ui-text) model-text
  ui-text)

(propagate (model-text) ui-text
  (format t "Invalidate UI: ~A~%" model-text)
  model-text)

(add-content model-text "lala")

(content model-text)
(content ui-text)
(content filled-text)

(add-content ui-text "")

(content model-text)
(content filled-text)

(add-content model-text "foo")

(content ui-text)
(content filled-text)

;; (show-graph model-text)
