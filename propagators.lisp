(in-package #:propagators)

(defvar *scheduler*)

(defun listify (thing)
  (if (listp thing)
      thing
      (list thing)))

(defstruct (nothing
             (:constructor nothing)))

(defstruct (contradiction
             (:constructor contradiction)))

(defclass cell ()
  ((name :initarg :name
         :accessor cell-name
         :type (or null symbol string)
         :initform nil)
   (merger :initarg :merger
           :accessor cell-merger
           :initform nil
           :type (or null symbol function)
           :documentation "Optional cell merger. If null, generic function MERGE is used.")
   (test :initarg :test
         :accessor cell-test
         :initform 'eq
         :type (or symbol function)
         :documentation "Predicate to be used for cell content comparison")
   (content :initarg :content
            :accessor content
            :initform (nothing)
            :documentation "The cell content")
   (neighbors :accessor neighbors
              :initform nil
              :documentation "Cell dependents")))

(defun make-cell (&rest args &key (type 'cell) &allow-other-keys)
  (apply #'make-instance type
         (alexandria:remove-from-plist args :type)))

(defmethod cell-p (object)
  nil)

(defmethod cell-p ((cell cell))
  t)

(defmethod add-content ((cell cell) increment &optional informant)
  (let ((new-content (if (cell-merger cell)
                         (funcall (cell-merger cell)
                                  (content cell)
                                  increment)
                         (merge-content cell (content cell) increment))))
    (cond
      ((funcall (cell-test cell)
                (content cell)
                new-content)
       :ok)
      ((contradiction-p new-content)
       (error "Contradiction"))
      (t (setf (content cell) new-content)
         (alert-propagators (neighbors cell) *scheduler*)))))

(defun add-neighbor (cell neighbor)
  (pushnew neighbor (neighbors cell))
  (alert-propagators neighbor *scheduler*))

(defmethod print-object ((cell cell) stream)
  (print-unreadable-object (cell stream :type t)
    (format stream "~@[~A: ~]~A"
            (cell-name cell)
            (content cell))))

(defun ensure-cell (thing)
  (if (cell-p thing)
      thing
      (make-cell :content thing)))

(defgeneric merge-content (cell content increment)
  (:method ((cell cell) content increment)
    (if (funcall (cell-test cell) content increment)
        content
        (error "Contradiction"))))

(defmethod merge-content ((cell cell) (content t) (increment nothing))
  content)

(defmethod merge-content ((cell cell) (content nothing) (increment t))
  increment)

(defmacro define-cell (name &optional (content (nothing)) &rest args)
  `(defparameter ,name
     (make-cell :content ,content
                :name ',name
                ,@args)))

(defmacro let-cells (bindings &body body)
  `(let ,(loop for binding in bindings
            collect
              (if (listp binding)
                  (destructuring-bind (var content &rest args) binding
                        (list var
                          `(make-cell :name ',var
                                      :content ,content
                                      ,@args)))
                  `(,binding (make-cell :name ',binding))))
     ,@body))

(defclass propagator ()
  ((neighbors :initarg :neighbors
              :accessor neighbors
              :initform nil)
   (output :initarg :output
           :accessor output
           :initform nil)
   (func :initarg :func
         :accessor propagator-func
         :initform (lambda () t))))

(defmethod initialize-instance :after ((propagator propagator) &rest initargs)
  (dolist (cell (neighbors propagator))
    (add-neighbor cell propagator))
  (alert-propagator propagator))

(defmethod print-object ((propagator propagator) stream)
  (print-unreadable-object (propagator stream :type t)
    (format stream "~A" (propagator-func propagator))))

(defun propagator-p (thing)
  (typep thing 'propagator))

(defun function->propagator-constructor (f)
  (lambda (&rest cells)
    (let ((output (ensure-cell (car (last cells))))
          (inputs (mapcar 'ensure-cell (butlast cells))))
      (flet ((the-propagator ()
               (add-content output
                            (apply f (mapcar 'content inputs)))))
        (make-instance
         'propagator
         :neighbors inputs
         :output output
         :func #'the-propagator)))))

(defun handling-nothings (f)
  (lambda (&rest args)
    (if (some 'nothing-p args)
        (nothing)
        (apply f args))))

(defmacro define-propagator-function (name fname)
  `(setf (symbol-function ',name)
         (function->propagator-constructor (handling-nothings #',fname))))

(defmacro propagate (inputs output &body body)
  `(make-instance 'propagator
                  :neighbors (list ,@inputs)
                  :output ,output
                  :func (lambda ()
                          (if (some 'nothing-p (mapcar 'content (list ,@inputs)))
                              (nothing)
                              (add-content
                               ,output
                               (let
                                   ,(loop for input in inputs
                                       collect `(,input (content ,input)))
                                 ,@body))))))

(defun square (x)
  (* x x))

(define-propagator-function adder +)
(define-propagator-function substractor -)
(define-propagator-function multiplier *)
(define-propagator-function divider /)
(define-propagator-function squarer square)
(define-propagator-function sqrter sqrt)

(defclass propagators-scheduler ()
  ((propagators-left :accessor propagators-left
                     :initform nil)
   (propagators-ever-alerted :accessor propagators-ever-alerted
                             :initform nil)))

(defclass round-robin-scheduler (propagators-scheduler)
  ())

(defclass reactive-scheduler (round-robin-scheduler)
  ())

(defun clear-scheduler (scheduler)
  (setf (propagators-left scheduler) nil))

(defmethod run-alerted ((scheduler round-robin-scheduler))
  (if (propagators-left scheduler)
      (let ((temp (propagators-left scheduler)))
        (setf (propagators-left scheduler) nil)
        (dolist (propagator temp)
          (execute-propagator propagator))
        (run-alerted scheduler))
      'done))

(defun execute-propagator (propagator)
  (funcall (propagator-func propagator)))

(defmethod alert-one (propagator (scheduler propagators-scheduler))
  (check-type propagator propagator)
  (push propagator (propagators-left scheduler))
  t)

(defmethod alert-propagators (propagators (scheduler propagators-scheduler))
  (dolist (propagator (listify propagators))
    (check-type propagator propagator)
    (push propagator (propagators-ever-alerted scheduler))
    (alert-one propagator scheduler)))

(defmethod alert-propagators :after (propagators (scheduler reactive-scheduler))
  (run-alerted scheduler))

(defun alert-propagator (&rest args)
  (apply #'alert-propagators (append args (list *scheduler*))))

(defun initialize-scheduler (&optional (type 'round-robin-scheduler) &rest args)
  (setf *scheduler* (apply #'make-instance type args)))

(defun run (&optional (scheduler *scheduler*))
  (run-alerted scheduler))

;;-- Reactive

(defclass reactive-cell (cell)
  ())

(defmethod merge-content ((cell reactive-cell) (content t) (increment nothing))
  content)

(defmethod merge-content ((cell reactive-cell) (content nothing) (increment t))
  increment)

(defmethod merge-content ((cell reactive-cell) content increment)
  ;; Just set the value for now
  increment)
